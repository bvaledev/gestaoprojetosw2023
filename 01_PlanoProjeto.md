---

Autor: Thiago Serra Ferreira de Carvalho
E-mail: thiago.carvalho (at) univag.edu.br
Data: 03.08.2023
TOCTitle: PlanoProjeto

---

# Plano do Projeto

## Grupo
<!--
    Isto é muito importante! A nota é do grupo, e todos precisam constar aqui, conforme exemplo,
nome completo, matrícula, curso e turma!
-->
- Jose da Silva Orlenas e Brangança Neto I, 88888, SIS 20/1
- Jose da Silva Orlenas e Brangança Neto I, 99999, SIS 20/1
- Jose da Silva Orlenas e Brangança Neto III, 000001, SIS 20/1
- Jose da Silva Orlenas e Brangança Neto IV, 332222, SIS 20/1


## Nome do Projeto
<!--
    Seja criativo mas, principalmente, coerente com o objeto do projeto.
--->
Um nome com uma palavra uma frase curta que descreva o projeto.


# Plano de Entrevistas
<!-- 
    Aqui devem estar um plano simples, exemplo a seguir, das entrevistas que amparam o projeto.
    Lembre-se que os interessados no projeto são as pessoas que devem ser entrevistadas. Ao menos duas.
-->

## Cronograma de entrevistas

| Data   | Entrevistado | Função na empresa | Perguntas importantes      |
| :------: | :------------ | :----------------- | :-------------------------- |
| 10.01.23 | João Silva | Diretor Financeiro| O que espera do projeto? Quais funções espera ver funcionando? |
| 12.01.23 | Roberto Silva | Gerente de Compras | Quais funções são importantes? O que tem maior priordade para sua áreas? |
| 14.01.23 | Sueli Silva   | Gerente de Vendas  | Qual a necessidade do setor? O que é mais importante neste momento? |


# Responsável pelo projeto
<!--
    Lembre-se as funções principais são:
    - Patrocinador: é a pessoa interessada pelo projeto, que tenha domínio do processo e seja capaz de definir ou decidir sobre o que será implementado.
    - Gerente do Projeto: é a pessoa que de fato gerenciará a equipe de projeto, no nosso exercício, apesar de haver um nome, todos no grupo exercem esse papel.
-->

O contratante estabelece as seguintes responsabilidades:
- Stakeholder principal: sr. João da Silva, Diretor Financeiro. 
- Gerente do Projeto: Consultoria contratada.


# Demanda inicial
<!--
    A partir do que ouviu nas entrevistas, qual é a demanda das áreas da empresa?
    O que elas eperam que o sistema entregue?
-->
Após ouvir os interessados, a demanda de TIC se trata de:
- Automação de atividade xyz E/OU
- Automação do processo de compra, conforme descrito a seguir: ... E/OU
- Construção de um sistema de controle de ... que seja capaz de ...

# Escopo
<!--
    Uma explicação clara e direta sobre o que o projeto pretende atender/resolver. Pode usar até dois parágrafos.
    Lembre-se, aqui estará o objetivo do projeto.
-->
O projeto tem como objetivo a entrega de uma solução que permita ... 


---
Ir para:
- [02 - Termo de Abertura >>](02_TermoAbertura.md)


